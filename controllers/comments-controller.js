const mongoose = require('mongoose');

const HttpError = require('../models/http-errors');
const { validationResult } = require('express-validator');
const catchAsync = require('../util/catch-async');
const User = require('../models/user');
const Place = require('../models/place');
const Comment = require('../models/comment');


exports.postComment = catchAsync(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new HttpError('Invalid inputs', 422));
  }
  const { description } = req.body;
  const placeId = req.params.pid;
  // Check for the creator id before persisting data
  let user;
  user = await User.findById(req.userData.userId);
  if (!user) {
    return next(new HttpError('We could not find the author', 404)); 
  }
  const place = await Place.findById(placeId);
  if (!place) return next(new HttpError('There is no place'));

   const postedComment = new Comment({
    description,
    author: req.userData.userId
   });
  
  const session = await mongoose.startSession();
  session.startTransaction();
  await postedComment.save({ session });
  await place.comments.push(postedComment); // The push method is from mongoose
  await place.save({ session });
  await session.commitTransaction();
  if (!postedComment) return next(new HttpError('Could not create comment', 500));
  res.status(201).json({ comments: postedComment });
});