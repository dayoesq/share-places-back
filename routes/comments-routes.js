const express = require('express');
const { check } = require('express-validator');
const checkAuth = require('../middlewares/check-auth');
const { postComment } = require('../controllers/comments-controller');
const router = express.Router();

router.use(checkAuth);

router.post('/:pid',
  [
    check('description').notEmpty()
  ], postComment
);

module.exports = router;